package org.nrg.dcm.xnat.exceptions;

public class UnableToBuildScanException extends Exception {
    public UnableToBuildScanException(String msg) {
        super(msg);
    }
}

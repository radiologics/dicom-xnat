/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.SOPMapXnatImagesessiondataBeanFactory
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.SOPClassUID;

import java.util.Set;

import org.nrg.dcm.SOPModel;
import org.slf4j.Logger;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public final class SOPMapXnatImagesessiondataBeanFactory extends
        AttributeMapXnatImagesessiondataBeanFactory {
    private final static Function<Set<String>,String> LEAD_SOP_EXTRACTOR = new Function<Set<String>,String>() {
        public String apply(final Set<String> sops) {
            return SOPModel.getSessionType(sops);
        }
    };
    
    /**
     * Creates a new instance of the factory class
     */
    public SOPMapXnatImagesessiondataBeanFactory() {
        super(SOPClassUID, SOPModel.getSOPClassToSessionTypes(), LEAD_SOP_EXTRACTOR);
    }
}

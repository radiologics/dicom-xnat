/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.DICOMScanBuilder
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.Modality;
import static org.nrg.dcm.Attributes.SOPClassUID;
import static org.nrg.dcm.Attributes.SeriesInstanceUID;
import static org.nrg.dcm.Attributes.SeriesNumber;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;

import lombok.val;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.EnumeratedMetadataStore;
import org.nrg.dcm.SOPModel;
import org.nrg.dcm.xnat.exceptions.UnableToBuildScanException;
import org.nrg.dcm.xnat.utils.DicomMappingUtils;
import org.nrg.io.RelativePathWriter;
import org.nrg.io.RelativePathWriterFactory;
import org.nrg.io.ScanCatalogFileWriterFactory;
import org.nrg.session.BeanBuilder;
import org.nrg.session.SessionBuilder;
import org.nrg.ulog.FileMicroLogFactory;
import org.nrg.ulog.MicroLog;
import org.nrg.util.FileURIOpener;
import org.nrg.xdat.bean.*;
import org.nrg.xdat.schema.SchemaElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public class DICOMScanBuilder implements Callable<XnatImagescandataBean> {
    private static final Logger logger = LoggerFactory.getLogger(DICOMScanBuilder.class);

    private final List<XnatImagescandataBeanFactory> scanBeanFactories = Lists.newArrayList();

    private final Map<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> scanBeanBuilders;
    private final Map<Class<? extends XnatImagescandataBean>, AttrDefs> scanTypeAttrs;

    private final static Class<? extends XnatImagescandataBeanFactory> DEFAULT_SCANDATABEAN_FACTORY =
            DefaultXnatImagescandataBeanFactory.class;

    private static final List<DicomAttributeIndex> ATTRIBUTES = Arrays.asList(SeriesInstanceUID, SeriesNumber, SOPClassUID, Modality);


    private final static Map<String, BeanBuilder> imageScanBeanBuilders = Collections.emptyMap();

    private final static ImmutableMap<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> defaultScanBeanBuilders =
            new ImmutableMap.Builder<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>>()
                    .put(XnatCrscandataBean.class, imageScanBeanBuilders)
                    .put(XnatCtscandataBean.class, new ImmutableMap.Builder<String, BeanBuilder>()
                            .putAll(imageScanBeanBuilders)
                            .putAll(CTScanAttributes.getBeanBuilders())
                            .build())
                    .put(XnatDxscandataBean.class, imageScanBeanBuilders)
                    .put(XnatDx3dcraniofacialscandataBean.class, imageScanBeanBuilders)
                    .put(XnatEcgscandataBean.class, imageScanBeanBuilders)
                    .put(XnatEpsscandataBean.class, imageScanBeanBuilders)
                    .put(XnatEsscandataBean.class, imageScanBeanBuilders)
                    .put(XnatEsvscandataBean.class, imageScanBeanBuilders)
                    .put(XnatGmscandataBean.class, imageScanBeanBuilders)
                    .put(XnatGmvscandataBean.class, imageScanBeanBuilders)
                    .put(XnatHdscandataBean.class, imageScanBeanBuilders)
                    .put(XnatIoscandataBean.class, imageScanBeanBuilders)
                    .put(XnatMgscandataBean.class, imageScanBeanBuilders)
                    .put(XnatMrscandataBean.class, imageScanBeanBuilders)
                    .put(XnatNmscandataBean.class, imageScanBeanBuilders)
                    .put(XnatOpscandataBean.class, imageScanBeanBuilders)
                    .put(XnatOptscandataBean.class, imageScanBeanBuilders)
                    .put(XnatPetscandataBean.class, imageScanBeanBuilders)
                    .put(XnatRfscandataBean.class, imageScanBeanBuilders)
                    .put(XnatRtimagescandataBean.class, imageScanBeanBuilders)
                    .put(XnatScscandataBean.class, imageScanBeanBuilders)
                    .put(XnatSegscandataBean.class, imageScanBeanBuilders)
                    .put(XnatSrscandataBean.class, imageScanBeanBuilders)
                    .put(XnatSmscandataBean.class, imageScanBeanBuilders)
                    .put(XnatUsscandataBean.class, imageScanBeanBuilders)
                    .put(XnatXcscandataBean.class, imageScanBeanBuilders)
                    .put(XnatXcvscandataBean.class, imageScanBeanBuilders)
                    .put(XnatOtherdicomscandataBean.class, imageScanBeanBuilders)
                    .build();

    private final static ImmutableMap<Class<? extends XnatImagescandataBean>, AttrDefs> defaultScanTypeAttrs =
            new ImmutableMap.Builder<Class<? extends XnatImagescandataBean>, AttrDefs>()
                    .put(XnatCrscandataBean.class, ImageScanAttributes.get())
                    .put(XnatCtscandataBean.class, CTScanAttributes.get())
                    .put(XnatDx3dcraniofacialscandataBean.class, ImageScanAttributes.get())
                    .put(XnatDxscandataBean.class, ImageScanAttributes.get())
                    .put(XnatEcgscandataBean.class, ImageScanAttributes.get())
                    .put(XnatEpsscandataBean.class, ImageScanAttributes.get())
                    .put(XnatEsscandataBean.class, ImageScanAttributes.get())
                    .put(XnatEsvscandataBean.class, ImageScanAttributes.get())
                    .put(XnatGmscandataBean.class, ImageScanAttributes.get())
                    .put(XnatGmvscandataBean.class, ImageScanAttributes.get())
                    .put(XnatHdscandataBean.class, ImageScanAttributes.get())
                    .put(XnatIoscandataBean.class, ImageScanAttributes.get())
                    .put(XnatMgscandataBean.class, ImageScanAttributes.get())
                    .put(XnatMrscandataBean.class, MRScanAttributes.get())
                    .put(XnatNmscandataBean.class, ImageScanAttributes.get())
                    .put(XnatOpscandataBean.class, ImageScanAttributes.get())
                    .put(XnatOptscandataBean.class, OPTScanAttributes.get())
                    .put(XnatPetscandataBean.class, PETScanAttributes.get())
                    .put(XnatRfscandataBean.class, ImageScanAttributes.get())
                    .put(XnatRtimagescandataBean.class, ImageScanAttributes.get())
                    .put(XnatScscandataBean.class, ImageScanAttributes.get())
                    .put(XnatSegscandataBean.class, ImageScanAttributes.get())
                    .put(XnatSrscandataBean.class, ImageScanAttributes.get())
                    .put(XnatSmscandataBean.class, ImageScanAttributes.get())
                    .put(XnatUsscandataBean.class, ImageScanAttributes.get())
                    .put(XnatXascandataBean.class, ImageScanAttributes.get())
                    .put(XnatXcscandataBean.class, ImageScanAttributes.get())
                    .put(XnatXcvscandataBean.class, ImageScanAttributes.get())
                    .put(XnatOtherdicomscandataBean.class, ImageScanAttributes.get())
                    .build();

    static ImmutableMap<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> getDefaultScanBeanBuilders() {
        return defaultScanBeanBuilders;
    }

    static ImmutableMap<Class<? extends XnatImagescandataBean>, AttrDefs> getDefaultScanTypeAttrs() {
        return defaultScanTypeAttrs;
    }

    static ImmutableSet<DicomAttributeIndex> getNativeTypeAttrs(Map<Class<? extends XnatImagescandataBean>, AttrDefs> allScanTypeAttrs) {
        return ImmutableSet.copyOf(
                Iterables.concat(Iterables.transform(allScanTypeAttrs.values(),
                        new Function<AttrDefs, Iterable<DicomAttributeIndex>>() {
                            public Iterable<DicomAttributeIndex> apply(final AttrDefs ad) {
                                return ad.getNativeAttrs();
                            }
                        })));
    }

    private final DicomMetadataStore                store;
    private final MicroLog                          ulog;
    private final String                            id;
    private final Series                            series;
    private final Iterable<XnatResourcecatalogBean> resources;
    private final Callable<Integer>                 nFrames;

    /**
     * NOTE: nFrames will always be evaluated AFTER an Iterator on catalogResource
     * has been fully consumed. Therefore, it's safe (and expected) to determine
     * the number of frames in the same code that builds the catalogs.
     *
     * @param store            DicomMetadataStore that holds metadata for the scan
     * @param sessionLog       MicroLog destination for warning and error reports
     * @param scanID           XNAT scan label
     * @param series           DICOM series record
     * @param catalogResources XnatResourcecatalogBeans representing the scan catalog(s)
     * @param nFrames          Callable that evaluates to the number of frames in the scan
     */
    public DICOMScanBuilder(final DicomMetadataStore store,
                            final MicroLog sessionLog,
                            final String scanID,
                            final Series series,
                            final Iterable<XnatResourcecatalogBean> catalogResources,
                            final Callable<Integer> nFrames,
                            final List<XnatImagescandataBeanFactory> scanBeanFactories,
                            final Map<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> scanBeanBuilders,
                            final Map<Class<? extends XnatImagescandataBean>, AttrDefs> scanTypeAttrs) {
        this.store = store;
        this.ulog = sessionLog;
        this.id = scanID;
        this.series = series;
        this.resources = catalogResources;
        this.nFrames = nFrames;
        if (scanBeanFactories != null)  {
            this.scanBeanFactories.addAll(scanBeanFactories);
        }
        this.scanBeanBuilders = scanBeanBuilders != null ? scanBeanBuilders : defaultScanBeanBuilders;
        this.scanTypeAttrs = scanTypeAttrs != null ? scanTypeAttrs : defaultScanTypeAttrs;
    }

    @Nullable
    private static XnatImagescandataBean createScanBean(List<XnatImagescandataBeanFactory> scanBeanFactories,
                                                        final DicomMetadataStore store,
                                                        final Series series) {
        for (final XnatImagescandataBeanFactory factory : scanBeanFactories) {
            final XnatImagescandataBean bean = factory.create(series, store);
            if (null != bean) {
                return bean;
            }
        }
        // Nothing worked; try to explain what happened.
        logger.error("Scan builder not implemented for SOP class {} or modalities {}",
                series.getSOPClasses(), series.getModalities());
        return null;
    }

    public XnatImagescandataBean call()
            throws IOException, SQLException, UnableToBuildScanException {
        final List<XnatImagescandataBeanFactory> scanBeanFactories = makeScanBeanFactories();
        final XnatImagescandataBean scan = createScanBean(scanBeanFactories, store, series);
        if (scan == null) {
            throw new UnableToBuildScanException("No suitable scan bean factories");
        }

        final AttrAdapter scanAttrs = new AttrAdapter(store, scanTypeAttrs.get(scan.getClass()));

        final Map<DicomAttributeIndex, String> scanSpec = Collections.singletonMap(SeriesInstanceUID, series.getUID());

        final Map<ExtAttrDef<DicomAttributeIndex>, Throwable> scanFailures = Maps.newHashMap();
        final List<ExtAttrValue> scanValues = org.nrg.session.SessionBuilder.getValues(scanAttrs, scanSpec, scanFailures);
        for (final Map.Entry<ExtAttrDef<DicomAttributeIndex>, Throwable> me : scanFailures.entrySet()) {
            DICOMSessionBuilder.report("scan " + id, me.getKey(), me.getValue(), ulog);
        }

        for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(scan, scanValues, scanBeanBuilders.get(scan.getClass()),
                                                                               "ID", "parameters/addParam")) {
            if ("ID".equals(val.getName())) {
                scan.setId(id);
            } else if ("parameters/addParam".equals(val.getName())) {
                final String paramName = val.getAttrs().get("name");
                try {
                    // This is nasty.  Is there a better way?
                    DicomMappingUtils.findAndInvokeAddParamMethod(scan, paramName, val.getText());
                } catch (Exception e) {
                    ulog.log(id, e);
                    logger.error("{} does not include a parameters/addParam field, unable to set {} for scan {}",
                            scan.getClass(), paramName, id, e);
                }
            } else {
                logger.error("scan field {} unexpectedly skipped", val.getName());
            }
        }

        for (final XnatResourcecatalogBean resource : resources) {
            scan.addFile(resource);
        }
        try {
            scan.setFrames(nFrames.call());
        } catch (Throwable t) {
            ulog.log("unable to determine frame count for scan " + id, t);
        }
        return scan;
    }

    private static File getScanDir(final DicomMetadataStore store, final Map<DicomAttributeIndex, String> scanSpec) throws IOException, SQLException {
        final AttrAdapter fileAttrs = new AttrAdapter(store, scanSpec);
        fileAttrs.add(ImageFileAttributes.get());
        final Multimap<URI, ExtAttrValue> unsorted = fileAttrs.getValuesForResources();
        if (unsorted.isEmpty()) {
            throw new FileNotFoundException("scan contains no image files");
        }
        return DICOMSessionBuilder.getCommonRoot(Lists.transform(Lists.newArrayList(unsorted.keySet()), new Function<URI, File>() {
            @Nullable
            @Override
            public File apply(@Nullable final URI input) {
                return input != null ? new File(input) : null;
            }
        }));
    }

    private static class ResourceCatalogCollector
            implements Callable<Iterable<XnatResourcecatalogBean>>, Iterable<XnatResourcecatalogBean> {
        private final DicomMetadataStore               store;
        private final MicroLog                         ulog;
        private final Map<DicomAttributeIndex, String> constraints;
        private final String                           scanID;
        private final RelativePathWriterFactory        catalogWriterFactory;
        private final boolean                          useRelativeCatalogPath;
        private Integer nFrames = null;

        ResourceCatalogCollector(final DicomMetadataStore store,
                                 final MicroLog ulog,
                                 final Map<DicomAttributeIndex, String> constraints,
                                 final String id,
                                 final RelativePathWriterFactory catalogWriterFactory,
                                 final boolean useRelativeCatalogPath) {
            this.store = store;
            this.ulog = ulog;
            this.constraints = constraints;
            this.scanID = id;
            this.catalogWriterFactory = catalogWriterFactory;
            this.useRelativeCatalogPath = useRelativeCatalogPath;
        }

        public Callable<Integer> getFrameCountCallable() {
            return new Callable<Integer>() {
                public Integer call() {
                    if (null == nFrames) {
                        throw new IllegalStateException("frame count not ready");
                    } else {
                        return nFrames;
                    }
                }
            };
        }

        public Iterable<XnatResourcecatalogBean> call() {
            try {
                final File scanDir = getScanDir(store, constraints);
                if (null == scanDir) {
                    ulog.log("No common root available for files in scan " + scanID);
                }
                final CatalogBuilder builder = new CatalogBuilder(scanID, ulog, store, scanDir, constraints);
                final Collection<XnatResourcecatalogBean> resources = Lists.newArrayList();
                for (final Map.Entry<XnatResourcecatalogBean, CatDcmcatalogBean> me : builder.call().entrySet()) {
                    final XnatResourcecatalogBean resource = me.getKey();
                    final CatDcmcatalogBean catalog = me.getValue();
                    final String label = "DICOM".equals(resource.getLabel()) ? scanID : scanID + "_" + resource.getLabel();
                    final RelativePathWriter writer = catalogWriterFactory.getWriter(scanDir, label);
                    try {
                        catalog.toXML(writer, true);
                    } finally {
                        writer.close();
                    }
                    resource.setUri(useRelativeCatalogPath ? writer.getRelativePath() : writer.getFullPath());
                    resources.add(resource);
                }
                nFrames = builder.getFrameCount();
                return resources;
            } catch (Throwable t) {
                final Logger logger = LoggerFactory.getLogger(DICOMScanBuilder.class);
                logger.error("Unable to build catalog for scan " + scanID, t);
                try {
                    ulog.log("Unable to build catalog for scan " + scanID, t);
                } catch (IOException e) {
                    logger.error("Unable to write to session log", e);
                }
                return Collections.emptyList();
            }
        }

        public Iterator<XnatResourcecatalogBean> iterator() {
            return call().iterator();
        }
    }

    private static Series buildSeries(final DicomMetadataStore store)
            throws IOException, SQLException {
        final Map<DicomAttributeIndex, ConversionFailureException> failures = Collections.emptyMap();
        final Set<Map<DicomAttributeIndex, String>> ids = store.getUniqueCombinations(ATTRIBUTES, failures);

        if (ids.isEmpty()) {
            throw new RuntimeException("no files found");    // TODO: better exception
        }

        final Multimap<DicomAttributeIndex, String> idsm = HashMultimap.create();
        for (final Map<DicomAttributeIndex, String> m : ids) {
            idsm.putAll(Multimaps.forMap(m));
        }

        final String type = SOPModel.getScanType(idsm.get(SOPClassUID));
        String seriesInstanceUID = null, seriesNumber = null;
        for (final Map<DicomAttributeIndex, String> m : ids) {
            if (m.containsKey(SeriesInstanceUID)) {
                seriesInstanceUID = m.get(SeriesInstanceUID);
            }
            if (m.containsKey(SeriesNumber)) {
                seriesNumber = m.get(SeriesNumber);
            }
            if (null == type || type.equals(SOPModel.getScanType(m.get(SOPClassUID)))) {
                break;
            }
        }

        final Series series = new Series(seriesNumber, seriesInstanceUID);
        for (final String modality : idsm.get(Modality)) {
            series.addModality(modality);
        }
        for (final String sopClass : idsm.get(SOPClassUID)) {
            series.addSOPClass(sopClass);
        }

        return series;
    }

    public static DICOMScanBuilder fromStore(final DicomMetadataStore store,
                                             final MicroLog sessionLog,
                                             final String scanID,
                                             final Series series,
                                             final Map<DicomAttributeIndex, String> constraints,
                                             final RelativePathWriterFactory catalogWriterFactory,
                                             final boolean useRelativePath,
                                             final List<XnatImagescandataBeanFactory> scanBeanFactoryClasses,
                                             final Map<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> scanBeanBuilders,
                                             final Map<Class<? extends XnatImagescandataBean>, AttrDefs> scanTypeAttrs) {
        final ResourceCatalogCollector collector = new ResourceCatalogCollector(store, sessionLog, constraints, scanID,
                catalogWriterFactory, useRelativePath);
        return new DICOMScanBuilder(store, sessionLog, scanID, series, collector,
                collector.getFrameCountCallable(), scanBeanFactoryClasses, scanBeanBuilders, scanTypeAttrs);
    }

    public static DICOMScanBuilder fromScanDirectory(final DicomMetadataStore store,
                                                     final MicroLog sessionLog,
                                                     final String scanID,
                                                     final RelativePathWriterFactory catalogWriterFactory,
                                                     final boolean useRelativePath,
                                                     final List<XnatImagescandataBeanFactory> scanBeanFactoryClasses,
                                                     final Map<Class<? extends XnatImagescandataBean>, Map<String, BeanBuilder>> scanBeanBuilders,
                                                     final Map<Class<? extends XnatImagescandataBean>, AttrDefs> scanTypeAttrs)
            throws IOException, SQLException {
        final Map<DicomAttributeIndex, String> constraints = Collections.emptyMap();
        final ResourceCatalogCollector collector = new ResourceCatalogCollector(store, sessionLog, constraints, scanID,
                catalogWriterFactory, useRelativePath);

        final Series series = buildSeries(store);

        return new DICOMScanBuilder(store, sessionLog, scanID, series, collector,
                collector.getFrameCountCallable(), scanBeanFactoryClasses, scanBeanBuilders, scanTypeAttrs);
    }

    public static XnatImagescandataBean buildScanFromDirectory(final File sessionDir,
                                                               final File scanDir,
                                                               final String scanID,
                                                               final boolean useRelativePath)
            throws IOException, SQLException, UnableToBuildScanException {
        final FileMicroLogFactory logFactory = new FileMicroLogFactory(sessionDir);
        final MicroLog log = logFactory.getLog("scan-" + scanDir.getName());
        final RelativePathWriterFactory catalogWriterFactory = new ScanCatalogFileWriterFactory(sessionDir);

        final Set<DicomAttributeIndex> attrs = Sets.newLinkedHashSet(getNativeTypeAttrs(defaultScanTypeAttrs));
        attrs.addAll(ImageFileAttributes.get().getNativeAttrs());
        attrs.addAll(CatalogAttributes.get().getNativeAttrs());
        attrs.addAll(Arrays.asList(SeriesInstanceUID, SeriesNumber, SOPClassUID, Modality));
        final DicomMetadataStore store = EnumeratedMetadataStore.createHSQLDBBacked(attrs, FileURIOpener.getInstance());
        IOException ioexception = null;
        try {
            store.add(Collections.singletonList(scanDir.toURI()));
            return fromScanDirectory(store, log, scanID, catalogWriterFactory, useRelativePath,
                    null, defaultScanBeanBuilders, defaultScanTypeAttrs).call();
        } catch (IOException e) {
            throw ioexception = e;
        } finally {
            try {
                store.close();
            } catch (IOException e) {
                throw null == ioexception ? e : ioexception;
            }
        }
    }


    /**
     * Build the default scan bean factories and add them to any spring-injected ones
     *
     * @return session bean factory instances
     */
    private List<XnatImagescandataBeanFactory> makeScanBeanFactories() {
        final List< XnatImagescandataBeanFactory> factories = new ArrayList<>(scanBeanFactories);
        // instantiate default factory and add to list
        try {
            factories.add(DEFAULT_SCANDATABEAN_FACTORY.getConstructor().newInstance());
        } catch (final RuntimeException e) {
            throw e;
        } catch (final Exception e) {
            // Every possible exception here is programmer error; don't use checked exceptions.
            throw new RuntimeException(e);
        }
        for (XnatImagescandataBeanFactory factory : factories) {
            factory.setLogger(logger);
        }
        return factories;
    }
}

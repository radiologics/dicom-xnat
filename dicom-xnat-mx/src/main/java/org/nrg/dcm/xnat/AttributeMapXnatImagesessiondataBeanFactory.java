/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.AttributeMapXnatImagesessiondataBeanFactory
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static org.nrg.dcm.Attributes.StudyInstanceUID;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class AttributeMapXnatImagesessiondataBeanFactory extends XnatImagesessiondataBeanFactory {
    private final DicomAttributeIndex attribute;
    private final Map<String,Class<? extends XnatImagesessiondataBean>> classes;
    private final Function<Set<String>,String> leadExtractor;
    
    public AttributeMapXnatImagesessiondataBeanFactory(final DicomAttributeIndex attribute,
            final Map<String,String> keysToDataTypes,
            final Function<Set<String>,String> leadExtractor) {
        this.attribute = attribute;
        this.leadExtractor = leadExtractor;
        this.classes = ImmutableMap.copyOf(Maps.transformValues(keysToDataTypes, XnatClassMapping.forBaseClass(XnatImagesessiondataBean.class)));
    }

    private static <K,C> C getInstance(final K key, final Map<? extends K,Class<? extends C>> classes) {
        if (null == key) {
            return null;
        }
        final Class<? extends C> clazz = classes.get(key);
        if (null == clazz) {
            return null;
        }
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.xnat.XnatImagesessiondataBeanFactory#create(org.nrg.dcm.DicomMetadataStore, java.lang.String)
     */
    @Override
    public XnatImagesessiondataBean create(DicomMetadataStore store, String studyInstanceUID) {
        return create(store, studyInstanceUID, null);
    }

    @Override
    public XnatImagesessiondataBean create(final DicomMetadataStore store, final String studyInstanceUID, final Map<String, String> parameters) {
        // Next try simple SOP class mapping
        SetMultimap<DicomAttributeIndex, String> values = getValues(store, ImmutableMap.of(StudyInstanceUID, studyInstanceUID),
                Collections.singleton(attribute));
        if (null == values) {
            return null;
        }
        final Set<String> v = values.get(attribute);
        if (null == v || v.isEmpty()) {
            return null;
        }
        return getInstance(leadExtractor.apply(v), classes);
    }
}
